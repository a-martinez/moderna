import numpy as np
import matplotlib.pyplot as plt
import os

HAlfa = 656.2795
Neon = 653.2882

elem = ["D","H","Na","Ne"]
X = np.array([None,None,None,None])
Y = np.array([None,None,None,None])

#Obtener intensidades vs posicion de la matriz
for i in range(np.size(elem)):
    dat = np.genfromtxt(elem[i]+".csv", skip_header=1, dtype='float', delimiter=",", usecols = (1, 2, 3))
    dat = dat[dat[:,0].argsort()]
    y = []
    x = []
    n = dat[0,0]
    p = []
    for j in range(np.size(dat[:,0])):
        if(dat[j,0]!=n):
            y.append(np.mean(np.array(p)))
            x.append(n)
            n=dat[j,0]
            p = []
        p.append(dat[j,2])
    y.append(np.mean(np.array(p)))
    x.append(n)
    Y[i]=np.array(y)
    X[i]=np.array(x)

#Maximos de lineas
PosMax = np.array([None,None,None,None,None,None])
for g in range(4):
    PosMax[g] = np.mean(X[g][Y[g].argsort()[-3:]])
PosMax[4] = np.mean(X[2][11:48][Y[2][11:48].argsort()[-3:]])
PosMax[5] = np.mean(X[2][190:230][Y[2][190:230].argsort()[-3:]])

#Calibrar escala
pix = abs(PosMax[3]-PosMax[1])
nm = abs(HAlfa-Neon)
u=HAlfa-(PosMax[1]*nm/pix)
X=((X*nm)/pix)+u
PosMax = ((PosMax*nm)/pix)+u


#************************* Graficas *****************************


#Deuterio
plt.plot(X[0],Y[0])
plt.grid(True)
plt.title("Deuterio")
plt.ylabel("Intensidad")
plt.xlabel("Distancia (nm)")
plt.savefig("Deuterio")
plt.close()

#Deuterio zoom
plt.scatter(X[0],Y[0])
plt.grid(True)
n=1
dxy = []
for i in range(np.size(X[0])):
    if(X[0][i]<656.12 and X[0][i]>656.07):
        dxy.append(str(n)+" (%.3f, %.3f)"%(X[0][i],Y[0][i]))
        plt.annotate(n, ((X[0][i],Y[0][i])))
        n=n+1
plt.xlim(656.07,656.12)
plt.title("Deuterio zoom")
plt.ylabel("Intensidad")
plt.xlabel("Distancia (nm)")
plt.savefig("Deuterio linea 1 zoom")
plt.close()

#Hidrogeo
plt.plot(X[1],Y[1])
plt.grid(True)
plt.title("Hidrogeno")
plt.ylabel("Intensidad")
plt.xlabel("Distancia (nm)")
plt.savefig("Hidrogeno")
plt.close()

#Hidrogeo zoom
plt.scatter(X[1],Y[1])
n=1
hxy = []
for i in range(np.size(X[1])):
    if(X[1][i]<656.30 and X[1][i]>656.25):
        hxy.append(str(n)+" (%.3f, %.3f)"%(X[1][i],Y[1][i]))
        plt.annotate(n, ((X[1][i],Y[1][i])))
        n=n+1
#    plt.annotate("(%.3f, %.3f)"%(X[1][i],Y[1][i]), ((X[1][i],Y[1][i])))
plt.grid(True)
plt.xlim(656.25,656.30)
plt.title("Hidrogeno zoom")
plt.ylabel("Intensidad")
plt.xlabel("Distancia (nm)")
plt.savefig("Hidrogeno zoom")
plt.close()


#Sodio
plt.plot(X[2],Y[2])
plt.grid(True)
plt.title("Sodio")
plt.ylabel("Intensidad")
plt.xlabel("Distancia (nm)")
plt.savefig("Sodio")
plt.close()

#Sodio zoom 1
plt.scatter(X[2],Y[2])
plt.grid(True)
plt.xlim(654.72,654.82)
plt.title("Sodio linea 1 zoom")
plt.ylabel("Intensidad")
plt.xlabel("Distancia (nm)")
plt.savefig("Sodio linea 1 zoom")
plt.close()

#Sodio zoom 2
plt.scatter(X[2],Y[2])
plt.grid(True)
plt.xlim(655.28,655.35)
plt.title("Sodio linea 2 zoom")
plt.ylabel("Intensidad")
plt.xlabel("Distancia (nm)")
plt.savefig("Sodio linea 2 zoom")
plt.close()

#Neon
plt.plot(X[3],Y[3])
plt.grid(True)
plt.title("Neon")
plt.ylabel("Intensidad")
plt.xlabel("Distancia (nm)")
plt.savefig("Neon")
plt.close()

#Todas
for k in range(4):
    plt.plot(X[k],Y[k],label=elem[k])
plt.title("Intensidad de lineas")
plt.ylabel("Intensidad")
plt.xlabel("Distancia (nm)")
plt.legend(loc=2)
plt.grid(True)
plt.savefig("Intensidad de lineas")
plt.close()

#Todas en posicion
plt.scatter(PosMax[0],1.,label=elem[0], c="b",s=70)
plt.scatter(PosMax[1],1.,label=elem[1], c="r",s=70)
plt.scatter(PosMax[3],1.,label=elem[3], c="y",s=70)
plt.scatter(PosMax[4],1.,label=elem[2]+" 1", c="c",s=70)
plt.scatter(PosMax[5],1.,label=elem[2]+" 2", c="k",s=70)


for g in range(5):
    n=1.0001
    if(g%2==0):
        n=0.9999
    plt.annotate("%.4f"%(PosMax[g]), ((PosMax[g]+0.006,n)))

plt.grid(True)
plt.title("Lineas")
plt.xlabel("Distancia (nm)")
plt.legend()
frame1 = plt.gca()
frame1.axes.get_yaxis().set_visible(False)
plt.savefig("Lineas")
plt.close()

#*********************** Guardar Datos **************************

text_file = open("DatosSistemaNuevo.txt", "w")
text_file.write("Datos lab - perfiles obtenidos con la tecnica: column average plot, where the x-axis represents the horizontal distance through the selection and the y-axis the vertically averaged pixel intensity."+os.linesep+os.linesep)
text_file.write("Posiciones lineas principales (nm), se promedio las posicion de los 3 datos con mas intensidad de cada pico"+os.linesep+os.linesep)
text_file.write("Deuterio: "+str(PosMax[0])+os.linesep)
text_file.write("Hidrogeno: "+str(PosMax[1])+os.linesep)
text_file.write("Sodio 1: "+str(PosMax[4])+os.linesep)
text_file.write("Sodio 2: "+str(PosMax[5])+os.linesep)
text_file.write("Neon: "+str(PosMax[3])+os.linesep+os.linesep)
text_file.write("Todos los datos"+os.linesep)
for o in range(np.size(X)):
    text_file.write(os.linesep+"Elemento: "+elem[o]+os.linesep+os.linesep)
    text_file.write("Posicion(nm);    Intensidad"+os.linesep)
    for e in range(np.size(X[o])):
        text_file.write(str(X[o][e])+"; "+str(Y[o][e])+os.linesep)
text_file.write(os.linesep+"Leyenda graficas Hidrogeno zoom y Deuterio zoom (X,Y)"+os.linesep+os.linesep)
text_file.write("Deuterio:"+os.linesep)
for i in dxy:
    text_file.write(i+os.linesep)
text_file.write(os.linesep+"Hidrogeno:"+os.linesep)
for i in hxy:
    text_file.write(i+os.linesep)
text_file.close()


