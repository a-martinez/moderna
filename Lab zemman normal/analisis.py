import numpy as np
import matplotlib.pylab as plt
import pyfits

Cadmio,CadmioBpara,CadmioBperp,Neon = (None,)*4
Imagenes = np.array([Cadmio,CadmioBpara,CadmioBperp,Neon])
Archivos = np.array(["Cd_10s.FIT","Cd_paral_10s.FIT","Cd_perp_10s.FIT","Ne_10s.FIT"])

def load(archivo):
    a = pyfits.open(archivo)
    return a[0].data.astype(float)

def graf(Imagen,n,nombre):
    plt.subplot(2,2,n+1)
    plt.title(nombre)
    plt.imshow(Imagen, cmap=plt.cm.gray)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)

plt.subplots_adjust(hspace=0.15,wspace=0.1)
for i in range(np.size(Imagenes)):
    Imagenes[i] = load(Archivos[i])
    graf(np.log(Imagenes[i]),i,Archivos[i])
plt.savefig("imagenes.pdf",dpi=500,bbox_inches='tight')
plt.close()
